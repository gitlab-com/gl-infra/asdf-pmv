# Deprecated: use [`asdf-gl-infra`](https://gitlab.com/gitlab-com/gl-infra/asdf-gl-infra) instead.

This project has been depecated. Please use <https://gitlab.com/gitlab-com/gl-infra/asdf-gl-infra> instead.

---------------------------------------------------------------

# asdf-pmv

As [asdf](https://github.com/asdf-vm/asdf) plugin for
[pmv](https://gitlab.com/gitlab-com/gl-infra/pmv).

## Installation

```shell
asdf plugin add pmv https://gitlab.com/gitlab-com/gl-infra/asdf-pmv.git
```

This plugin installs a binary named `pmv`.

Requires:

* curl
* jq
