#!/usr/bin/env bash
set -euo pipefail

if [ "${ASDF_INSTALL_TYPE}" != "version" ]; then
  echo "ASDF_INSTALL_TYPE '${ASDF_INSTALL_TYPE}' not supported"
fi

mkdir -p "${ASDF_DOWNLOAD_PATH}"

if [[ $OSTYPE == "linux-gnu"* ]]; then
  operating_system="Linux"
elif [[ $OSTYPE == "darwin"* ]]; then
  operating_system="Darwin"
fi

case "$(uname -m)" in
aarch64_be | aarch64 | armv8b | armv8l | arm64)
  arch="arm64"
  ;;

*)
  arch="x86_64"
  ;;
esac

release_json=$(curl --fail --silent "https://gitlab.com/api/v4/projects/34872266/releases/v${ASDF_INSTALL_VERSION}")

find_download() {
  jq -r --arg arch "$1" --arg ostype "$2" '
  [
    .assets.links[] |
    select(.name|ascii_downcase|contains("_\($ostype)_"|ascii_downcase)) |
    select(.name|ascii_downcase|contains("_\($arch)"|ascii_downcase))
  ] |
  first|
  .url
'
}

download_url="$(echo "$release_json" | find_download "$arch" "$operating_system")"

if [[ -z $download_url ]]; then
  # Fall back to x86_64
  download_url="$(echo "$release_json" | find_download "x86_64" "$operating_system")"
fi

if [[ -z $download_url ]]; then
  echo "Unable to find a download for operating system '$OSTYPE', architecture '$(uname -m)'"
  exit 1
fi

curl --fail --location --silent --output \
  "${ASDF_DOWNLOAD_PATH}/pmv-v${ASDF_INSTALL_VERSION}.tar.gz" \
  "${download_url}" || {
  echo "❌ Failed to download the pmv package bundle from ${download_url}"
  exit 1
}

# Remove the quarantine flag
if [[ $OSTYPE == "darwin"* ]]; then
  (xattr -d com.apple.quarantine "${ASDF_DOWNLOAD_PATH}/pmv-v${ASDF_INSTALL_VERSION}.tar.gz" >/dev/null 2>&1) || true
fi
